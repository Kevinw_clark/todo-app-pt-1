//suri helped me drill the props down to the children componenets. Thanks!

import React, { useState } from "react";
import todosList from "./todos.json";

function App () { 
  const [todos, setTodos] = useState(todosList)
  const [inputValue, setInputValue] = useState('')
  
  function handleNewTodoChange(e) {
    if(e.key === "Enter" && e.target.value !== ""){
    setTodos([...todos, 
        {
          "userId": 1,
          "id": Date.now(),
          "title": e.target.value,
          "completed": false
        },
      ])
      setInputValue('')
    }
  }

  function handleChange(event) {
    setInputValue(event.target.value)
  }

  function toggleComplete(id) {
    let newTodos = todos.map(todo => { 
      if(todo.id === id){
        return {...todo, completed: !todo.complete}
      }
      return todo
     })
     setTodos(newTodos)
  }

  function destroyTodo(id) {
    let newTodos = todos.filter(todo => { 
      return todo.id !== id
     })
     setTodos(newTodos)
     console.log(newTodos)
  }

  function clearCompleted() {
    let newTodos = todos.filter(todo => {
      return todo.completed === false
    })
    setTodos(newTodos)
  }

  return (
    <section className="todoapp">

        <header className="header">
          <h1>enter a todo</h1>
          <input 
            onKeyPress={handleNewTodoChange}
            className="new-todo" 
            placeholder="What needs to be done?" 
            autoFocus
            onChange={handleChange}
            value={inputValue}
          />
        </header>

      <TodoList destroyTodo={destroyTodo} toggleComplete={toggleComplete} todos={todos} />

        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button onClick={clearCompleted} className="clear-completed">Clear completed</button>
        </footer>

    </section>
  );
}

function TodoItem( props ) {
  return (
    <li className={props.completed ? "completed" : ""}>
      <div className="view">
        <input onChange={props.toggleComplete} className="toggle" type="checkbox" checked={props.completed} />
        <label>{props.title}</label>
        <button onClick={props.destroyTodo} className="destroy" />
      </div>
    </li>
  );
}

function TodoList( props ) {
  return (
    <section className="main">
      <ul className="todo-list">
        {props.todos.map((todo) => {
          return(
          <TodoItem 
            destroyTodo={event => props.destroyTodo(todo.id)}
            toggleComplete={event => props.toggleComplete(todo.id)} 
            key={todo.id} 
            title={todo.title} 
            completed={todo.completed} />
        )}
        )}
      </ul>
    </section>
  );
}


export default App;
